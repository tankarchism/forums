# Throwback Forums

## Getting Started

### Requirements

* PHP >= 8.0
* Composer >= 2.0

### Installation

Download and install the [Symfony command line tools](https://symfony.com/download).

Open up a command line in the installation directory and run:

	composer install

Afterward, make a copy of `.env` to `.env.local` and change the values
within this copy accordingly.
